/*
 * ConfigHandler.c
 *
 *  Created on: Oct 29, 2023
 *      Author: johannes
 */

#include "ConfigHandler.h"

void initPin(Pin* p_pin, const char *pin_data)
{
	// parse id
	p_pin->id = pin_data[PIN_ID_OFFSET];

	// pin parsing / mapping
	switch(pin_data[PIN_PIN_OFFSET])
	{
		case 0:  p_pin->initTypedef.Pin = GPIO_PIN_0;  break;
		case 1:  p_pin->initTypedef.Pin = GPIO_PIN_1;  break;
		case 2:  p_pin->initTypedef.Pin = GPIO_PIN_2;  break;
		case 3:  p_pin->initTypedef.Pin = GPIO_PIN_3;  break;
		case 4:  p_pin->initTypedef.Pin = GPIO_PIN_4;  break;
		case 5:  p_pin->initTypedef.Pin = GPIO_PIN_5;  break;
		case 6:  p_pin->initTypedef.Pin = GPIO_PIN_6;  break;
		case 7:  p_pin->initTypedef.Pin = GPIO_PIN_7;  break;
		case 8:  p_pin->initTypedef.Pin = GPIO_PIN_8;  break;
		case 9:  p_pin->initTypedef.Pin = GPIO_PIN_9;  break;
		case 10: p_pin->initTypedef.Pin = GPIO_PIN_10; break;
		case 11: p_pin->initTypedef.Pin = GPIO_PIN_11; break;
		case 12: p_pin->initTypedef.Pin = GPIO_PIN_12; break;
		case 13: p_pin->initTypedef.Pin = GPIO_PIN_13; break;
		case 14: p_pin->initTypedef.Pin = GPIO_PIN_14; break;
		case 15: p_pin->initTypedef.Pin = GPIO_PIN_15; break;
		case 255: p_pin->initTypedef.Pin = GPIO_PIN_All; break;
		default: /* not good */ break;
	}

	// port parsing / mapping
	switch(pin_data[PIN_PORT_OFFSET])
	{
		case 'A': p_pin->port = GPIOA;	break;
		case 'B': p_pin->port = GPIOB;	break;
		case 'C': p_pin->port = GPIOC;	break;
		default: /* not good */ break;
	}

	// parse Mode
	switch(pin_data[PIN_MODE_OFFSET])
	{
		case 'O': p_pin->initTypedef.Mode = GPIO_MODE_OUTPUT_PP; break;
		case 'I': p_pin->initTypedef.Mode = GPIO_MODE_INPUT; break;
		// TODO other mode options

		default: /* not good */ break;
	}

	// parse Pull
	switch(pin_data[PIN_MODE_OFFSET])
	{
		case 'U': p_pin->initTypedef.Pull = GPIO_PULLUP; break;
		case 'D': p_pin->initTypedef.Pull = GPIO_PULLDOWN; break;
		default: p_pin->initTypedef.Pull = GPIO_NOPULL; break;
	}

	// parse Speed
	switch(pin_data[PIN_MODE_OFFSET])
	{
		case 'L': p_pin->initTypedef.Speed = GPIO_SPEED_FREQ_LOW; break;
		case 'M': p_pin->initTypedef.Speed = GPIO_SPEED_FREQ_MEDIUM; break;
		case 'H': p_pin->initTypedef.Speed = GPIO_SPEED_FREQ_HIGH; break;
		case 'V': p_pin->initTypedef.Speed = GPIO_SPEED_FREQ_VERY_HIGH; break;
		default: p_pin->initTypedef.Speed = GPIO_SPEED_FREQ_LOW; break;
	}

	return;
}

void initConfig(Config* p_config, const char *rx_data)
{
	// TODO version control


	// parse metadata
	p_config->id = rx_data[0];
	p_config->pin_count = rx_data[1];

	// create pins
	p_config->pins = (Pin*)malloc(sizeof(Pin) * p_config->pin_count);

	// parse pin data
	for (uint8_t i = 0; i < p_config->pin_count; ++i) {

		char pin_data[6];
		// copy 6 bytes
		strncpy(pin_data, rx_data + i*PIN_DATA_LENGTH, PIN_DATA_LENGTH);

		initPin(&p_config->pins[i], pin_data);
	}

	return;
}

void initPinsFromConfig(Config *p_config)
{
	for(uint8_t i = 0; i < p_config->pin_count; ++i)
	{
		HAL_GPIO_Init(p_config->pins[i].port, &p_config->pins[i].initTypedef);
	}

	return;
}
