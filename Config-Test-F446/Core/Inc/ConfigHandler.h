/*
 * ConfigHandler.h
 *
 *  Created on: Oct 29, 2023
 *      Author: johannes
 */

#ifndef INC_CONFIGHANDLER_H_
#define INC_CONFIGHANDLER_H_

#include <string.h>
#include <stdlib.h>

#include "stm32f4xx_hal.h"

#define ID_OFFSET 0
#define PIN_COUNT_OFFSET 1
#define PIN_OFFSET 2

#define PIN_ID_OFFSET 0
#define PIN_PIN_OFFSET 1
#define PIN_PORT_OFFSET 2
#define PIN_MODE_OFFSET 3
#define PIN_PULL_OFFSET 4
#define PIN_SPEED_OFFSET 5

#define PIN_DATA_LENGTH 6

typedef struct {

	uint8_t id;
	char* label;

	GPIO_InitTypeDef initTypedef;
	GPIO_TypeDef* port;

} Pin;

typedef struct {

	uint8_t id;
	uint8_t pin_count;

	Pin *pins;

} Config;

void initConfig(Config* p_config, const char *rx_data);
void initPinsFromConfig(Config *p_config);


#endif /* INC_CONFIGHANDLER_H_ */
