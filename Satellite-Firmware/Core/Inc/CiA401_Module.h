/*
 * CiA401.h
 *
 *  Created on: Nov 30, 2023
 *      Author: Johannes Hofmann
 */

#ifndef INC_CIA401_MODULE_H_
#define INC_CIA401_MODULE_H_

#include "stm32f4xx_hal.h"
#include "CO_app_STM32.h"

#include "Pin_Manager.h"
#include "configuration.h"

extern PinManager_t pin_manager;
extern CANopenNodeSTM32 canOpenNodeSTM32;

#ifdef DIGITAL_INPUT_ENABLE
void CiA401_ProcessDigitalInput(void);
#endif
#ifdef DIGITAL_OUTPUT_ENABLE
void CiA401_ProcessDigitalOutput(void);
#endif
#ifdef ANALOG_INPUT_ENABLE
void CiA401_ProcessAnalogInput(void);
#endif
#ifdef ANALOG_OUTPUT_ENABLE
void CiA401_ProcessAnalogOutput(void);
#endif

#endif /* INC_CIA401_MODULE_H_ */
