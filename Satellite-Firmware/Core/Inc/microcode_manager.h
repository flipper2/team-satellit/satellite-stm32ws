/*
 * microcode_manager.h
 *
 *  Created on: Dec 10, 2023
 *      Author: johannes
 */

#ifndef INC_MICROCODE_MANAGER_H_
#define INC_MICROCODE_MANAGER_H_

#include <stdlib.h>
#include "stm32f4xx_hal.h"

#include "CANopen.h"
#include "OD.h"

#include "gpioasm.h"
#include "timer.h"

#include "configuration.h"

typedef struct {

	uint32_t size;

	gpioasm_engine_t engines[MICROCODE_ENGINE_COUNT];
	Timer_t timers[MICROCODE_ENGINE_COUNT];


} MicrocodeManager_t;

void MicrocodeManager_init(MicrocodeManager_t *_microcode_manager, TIM_HandleTypeDef *timer_handle, gpioasm_engine_init_t *_gpioasm_init, uint32_t _engine_count);

void MicrocodeManager_start(MicrocodeManager_t *_microcode_manager, uint32_t index);
void MicrocodeManager_stop(MicrocodeManager_t *_microcode_manager, uint32_t index);
void MicrocodeManager_restart(MicrocodeManager_t *_microcode_manager, uint32_t index);


#endif /* INC_MICROCODE_MANAGER_H_ */
