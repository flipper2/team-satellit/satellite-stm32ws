/*
 * Pin.h
 *
 *  Created on: Nov 29, 2023
 *      Author: Johannes Hofmann
 */

#ifndef INC_PIN_MANAGER_H_
#define INC_PIN_MANAGER_H_

// includes
#include <stdlib.h>
#include <string.h>

#include "stm32f4xx_hal.h"

#include "301/CO_ODinterface.h"
#include "configuration.h"

extern OD_t *OD;

// defines
#ifdef DIGITAL_INPUT_ENABLE
/// Digital Input 1-bit mode
#define OD_DIGITAL_INPUT_ENTRY 			(uint16_t)0x6020
#define OD_DIGITAL_INPUT_POLARITY 		(uint16_t)0x6030
#define OD_DIGITAL_INPUT_FILTER 		(uint16_t)0x6038
#define OD_DIGITAL_INPUT_IT_ENABLE 		(uint16_t)0x6005
#define OD_DIGITAL_INPUT_IT_BOTH		(uint16_t)0x6050
#define OD_DIGITAL_INPUT_IT_FALLING		(uint16_t)0x6070
#define OD_DIGITAL_INPUT_IT_RISING		(uint16_t)0x6060
#endif

#ifdef DIGITAL_OUTPUT_ENABLE
/// Digital Output 1-bit mode
#define OD_DIGITAL_OUTPUT_ENTRY 		(uint16_t)0x6220
#define OD_DIGITAL_OUTPUT_POLARITY 		(uint16_t)0x6240
#define OD_DIGITAL_OUTPUT_ERROR_MODE 	(uint16_t)0x6250
#define OD_DIGITAL_OUTPUT_ERROR_VALUE 	(uint16_t)0x6260
#define OD_DIGITAL_OUTPUT_FILTER 		(uint16_t)0x6270
#endif

#ifdef ANALOG_INPUT_ENABLE
// Analog Input
#define OD_ANALOG_OUTPUT_VALUE_8				(uint16_t)0x6410
#define OD_ANALOG_OUTPUT_VALUE_16				(uint16_t)0x6411
#define OD_ANALOG_OUTPUT_VALUE_32				(uint16_t)0x6412
#define OD_ANALOG_OUTPUT_VALUE_FLOAT			(uint16_t)0x6413
#define OD_ANALOG_OUTPUT_OFFSET_FLOAT			(uint16_t)0x6441
#define OD_ANALOG_OUTPUT_SCALING_FLOAT			(uint16_t)0x6442
#define OD_ANALOG_OUTPUT_OFFSET_INTEGER			(uint16_t)0x6446
#define OD_ANALOG_OUTPUT_SCALING_INTEGER		(uint16_t)0x6447
#define OD_ANALOG_OUTPUT_ERROR_MODE				(uint16_t)0x6443
#define OD_ANALOG_OUTPUT_ERROR_VALUE_FLOAT		(uint16_t)0x6445
#define OD_ANALOG_OUTPUT_ERROR_VALUE_INTEGER	(uint16_t)0x6444
#endif

#ifdef ANALOG_OUTPUT_ENABLE
// Analog Output
#define OD_ANALOG_OUTPUT_VALUE_8				(uint16_t)0x6410
#define OD_ANALOG_OUTPUT_VALUE_16				(uint16_t)0x6411
#define OD_ANALOG_OUTPUT_VALUE_32				(uint16_t)0x6412
#define OD_ANALOG_OUTPUT_VALUE_FLOAT			(uint16_t)0x6413
#define OD_ANALOG_OUTPUT_OFFSET_FLOAT			(uint16_t)0x6441
#define OD_ANALOG_OUTPUT_SCALING_FLOAT			(uint16_t)0x6442
#define OD_ANALOG_OUTPUT_OFFSET_INTEGER			(uint16_t)0x6446
#define OD_ANALOG_OUTPUT_SCALING_INTEGER		(uint16_t)0x6447
#define OD_ANALOG_OUTPUT_ERROR_MODE				(uint16_t)0x6443
#define OD_ANALOG_OUTPUT_ERROR_VALUE_FLOAT		(uint16_t)0x6445
#define OD_ANALOG_OUTPUT_ERROR_VALUE_INTEGER	(uint16_t)0x6444
#endif

// typedefs
typedef struct
{
	// GPIO
	uint16_t GPIO_Pin;
	GPIO_TypeDef *GPIO_Port;

} Pin_t;

typedef struct
{
	GPIO_InitTypeDef GPIO_InitPin;
	GPIO_TypeDef *GPIO_Port;

} Pin_InitTypeDef;

#ifdef DIGITAL_INPUT_ENABLE
typedef struct {
	Pin_t pin;

	uint8_t sub_index;

	uint8_t *p_input_entry;
	uint8_t *p_input_filter;
	uint8_t *p_input_polarity;
	bool_t  *p_input_it_enable;
	uint8_t *p_input_it_both;
	uint8_t *p_input_it_rising;
	uint8_t *p_input_it_falling;

} DigitalInput_t;
#endif

#ifdef DIGITAL_OUTPUT_ENABLE
typedef struct {
	Pin_t pin;

	uint8_t sub_index;

	uint8_t *p_output_entry;
	uint8_t *p_output_polarity;
	uint8_t *p_output_filter;
	uint8_t *p_output_error_mode;
	uint8_t *p_output_error_value;

} DigitalOutput_t;
#endif

#ifdef ANALOG_INPUT_ENABLE
typedef struct {
	Pin_t pin;
	//...

} AnalogInput_t;
#endif

#ifdef ANALOG_OUTPUT_ENABLE
typedef struct {

	// GPIO Pin
	Pin_t pin;

	// Timer
	TIM_HandleTypeDef *htim;
	uint32_t channel;

	// CANOpen
	uint8_t sub_index;

	int8_t *p_value_8;
	int16_t *p_value_16;
	int32_t *p_value_32;
	float32_t *p_value_float;

	int16_t old_value_8;

	int32_t *p_offset_32;
	int32_t *p_scaling_32;
	float32_t *p_offset_float;
	float32_t *p_scaling_float;

	uint8_t *p_error_mode;
	uint32_t *p_error_value_32;
	float32_t *p_error_value_float;

} AnalogOutput_t;
#endif

typedef struct
{
#ifdef DIGITAL_INPUT_ENABLE
	DigitalInput_t *digital_input;
	uint32_t size_digital_input;
#endif
#ifdef DIGITAL_OUTPUT_ENABLE
	DigitalOutput_t *digital_output;
	uint32_t size_digital_output;
#endif
#ifdef ANALOG_INPUT_ENABLE
	AnalogOutput_t *analog_input;
	uint32_t size_analog_input;
#endif
#ifdef ANALOG_OUTPUT_ENABLE
	AnalogOutput_t *analog_output;
	uint32_t size_analog_output;
#endif
} PinManager_t;

// Pin Manager
void PinManager_init(PinManager_t *_pin_manager);
void PinManager_delete(PinManager_t* pin_manager);

#ifdef DIGITAL_INPUT_ENABLE
void PinManager_addDigitalInputPin(PinManager_t *_pin_manager, Pin_t _pin);
GPIO_PinState PinManager_readDigitalInputPin(PinManager_t *_pin_manager, uint32_t index);
#endif

#ifdef DIGITAL_OUTPUT_ENABLE
void PinManager_addDigitalOutputPin(PinManager_t *_pin_manager, Pin_t _pin);
void PinManager_writeDigitalOutputPin(PinManager_t *_pin_manager, uint32_t index, GPIO_PinState state);
#endif

#ifdef ANALOG_INPUT_ENABLE
void PinManager_addAnalogInputPin(PinManager_t *_pin_manager, Pin_t _pin);
uint32_t PinManager_readAnalogInputPin(PinManager_t *_pin_manager, uint32_t index);
#endif

#ifdef ANALOG_OUTPUT_ENABLE
void PinManager_addAnalogOutputPin(PinManager_t *_pin_manager, TIM_HandleTypeDef *htim, uint32_t channel);
void PinManager_writeAnalogOutputPin(PinManager_t *_pin_manager, uint32_t index, uint16_t duty_cycle);
#endif

#endif /* INC_PIN_MANAGER_H_ */







