/*
 * configuration.h
 *
 *  Created on: Dec 11, 2023
 *      Author: johannes
 */

#ifndef INC_CONFIGURATION_H_
#define INC_CONFIGURATION_H_

#define LOG_DEBUG(...)
#define LOG_ERROR(...)

// Debug
#define DEBUG_PIN_MANAGER
#define DEBUG_CIA401
#define DEBUG_MICROCODE

// IO Capabilities
#define DIGITAL_INPUT_ENABLE
#define DIGITAL_OUTPUT_ENABLE
//#define ANALOG_INPUT_ENABLE
#define ANALOG_OUTPUT_ENABLE


#define MICROCODE_ENGINE_COUNT 16

#endif /* INC_CONFIGURATION_H_ */
