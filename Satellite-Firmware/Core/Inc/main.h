/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GPIO_6_Pin GPIO_PIN_13
#define GPIO_6_GPIO_Port GPIOC
#define GPIO_15_Pin GPIO_PIN_14
#define GPIO_15_GPIO_Port GPIOC
#define GPIO_16_Pin GPIO_PIN_15
#define GPIO_16_GPIO_Port GPIOC
#define GPIO_14_Pin GPIO_PIN_0
#define GPIO_14_GPIO_Port GPIOC
#define GPIO_4_Pin GPIO_PIN_1
#define GPIO_4_GPIO_Port GPIOC
#define GPIO_13_Pin GPIO_PIN_2
#define GPIO_13_GPIO_Port GPIOC
#define GPIO_5_Pin GPIO_PIN_3
#define GPIO_5_GPIO_Port GPIOC
#define GPIO_3_Pin GPIO_PIN_0
#define GPIO_3_GPIO_Port GPIOA
#define GPIO_11_Pin GPIO_PIN_1
#define GPIO_11_GPIO_Port GPIOA
#define GPIO_12_Pin GPIO_PIN_2
#define GPIO_12_GPIO_Port GPIOA
#define GPIO_10_Pin GPIO_PIN_3
#define GPIO_10_GPIO_Port GPIOA
#define GPIO_9_Pin GPIO_PIN_4
#define GPIO_9_GPIO_Port GPIOA
#define GPIO_0_Pin GPIO_PIN_5
#define GPIO_0_GPIO_Port GPIOA
#define GPIO_2_Pin GPIO_PIN_6
#define GPIO_2_GPIO_Port GPIOA
#define GPIO_1_Pin GPIO_PIN_7
#define GPIO_1_GPIO_Port GPIOA
#define LED_RED_Pin GPIO_PIN_4
#define LED_RED_GPIO_Port GPIOC
#define LED_GREEN_Pin GPIO_PIN_5
#define LED_GREEN_GPIO_Port GPIOC
#define CAN_SILENT_Pin GPIO_PIN_12
#define CAN_SILENT_GPIO_Port GPIOB
#define COIL_1_Pin GPIO_PIN_14
#define COIL_1_GPIO_Port GPIOB
#define COIL_0_Pin GPIO_PIN_15
#define COIL_0_GPIO_Port GPIOB
#define COIL_2_Pin GPIO_PIN_6
#define COIL_2_GPIO_Port GPIOC
#define COIL_3_Pin GPIO_PIN_7
#define COIL_3_GPIO_Port GPIOC
#define COIL_4_Pin GPIO_PIN_8
#define COIL_4_GPIO_Port GPIOC
#define COIL_5_Pin GPIO_PIN_9
#define COIL_5_GPIO_Port GPIOC
#define COIL_7_Pin GPIO_PIN_8
#define COIL_7_GPIO_Port GPIOA
#define COIL_6_Pin GPIO_PIN_11
#define COIL_6_GPIO_Port GPIOA
#define GPIO_8_Pin GPIO_PIN_10
#define GPIO_8_GPIO_Port GPIOC
#define GPIO_17_Pin GPIO_PIN_11
#define GPIO_17_GPIO_Port GPIOC
#define GPIO_7_Pin GPIO_PIN_12
#define GPIO_7_GPIO_Port GPIOC
#define ID_2_Pin GPIO_PIN_2
#define ID_2_GPIO_Port GPIOD
#define ID_1_Pin GPIO_PIN_3
#define ID_1_GPIO_Port GPIOB
#define ID_0_Pin GPIO_PIN_4
#define ID_0_GPIO_Port GPIOB
#define COIL_8_Pin GPIO_PIN_6
#define COIL_8_GPIO_Port GPIOB
#define COIL_9_Pin GPIO_PIN_7
#define COIL_9_GPIO_Port GPIOB
#define COIL_10_Pin GPIO_PIN_8
#define COIL_10_GPIO_Port GPIOB
#define COIL_11_Pin GPIO_PIN_9
#define COIL_11_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
