/*
 * microcode_manager.c
 *
 *  Created on: Dec 10, 2023
 *      Author: johannes
 */

#include "microcode_manager.h"

bool_t update_microcode_from_OD(MicrocodeManager_t *_microcode_manager, uint32_t index)
{
	switch(index)
	{
	case 0: 	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_0, sizeof(OD_RAM.x7000_microcode.microcode_0)); break;
	case 1: 	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_1, sizeof(OD_RAM.x7000_microcode.microcode_1)); break;
	case 2: 	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_2, sizeof(OD_RAM.x7000_microcode.microcode_2)); break;
	case 3: 	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_3, sizeof(OD_RAM.x7000_microcode.microcode_3)); break;
	case 4: 	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_4, sizeof(OD_RAM.x7000_microcode.microcode_4)); break;
	case 5: 	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_5, sizeof(OD_RAM.x7000_microcode.microcode_5)); break;
	case 6:		gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_6, sizeof(OD_RAM.x7000_microcode.microcode_6)); break;
	case 7:		gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_7, sizeof(OD_RAM.x7000_microcode.microcode_7)); break;
	case 8:		gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_8, sizeof(OD_RAM.x7000_microcode.microcode_8)); break;
	case 9:		gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_9, sizeof(OD_RAM.x7000_microcode.microcode_9)); break;
	case 10:	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_10, sizeof(OD_RAM.x7000_microcode.microcode_10)); break;
	case 11:	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_11, sizeof(OD_RAM.x7000_microcode.microcode_11)); break;
	case 12:	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_12, sizeof(OD_RAM.x7000_microcode.microcode_12)); break;
	case 13:	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_13, sizeof(OD_RAM.x7000_microcode.microcode_13)); break;
	case 14:	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_14, sizeof(OD_RAM.x7000_microcode.microcode_14)); break;
	case 15:	gpioasm_push_packet(_microcode_manager->engines + index, OD_RAM.x7000_microcode.microcode_15, sizeof(OD_RAM.x7000_microcode.microcode_15)); break;
	default: return false;
	}

	//	for (uint32_t i = 0; i < ENGINE_COUNT; ++i) {
	//		gpioasm_push_packet(_ga_engines+i, &OD_RAM.x7000_microcode[i+1], OD_RAM.x7001_microcodeLength[i+1]);
	//	}
	return true;
}

void MicrocodeManager_init(MicrocodeManager_t *_microcode_manager, TIM_HandleTypeDef *timer_handle, gpioasm_engine_init_t *_gpioasm_init, uint32_t _engine_count)
{
#ifdef DEBUG_MICROCODE
	LOG_DEBUG("Microcode manager: Init function called\r\n");
#endif

	// memory allocation
	_microcode_manager->size = _engine_count;
//	_microcode_manager->engines = (gpioasm_engine_t*)malloc(_microcode_manager->size * sizeof(gpioasm_engine_t));
//	_microcode_manager->timers = (Timer_t*)malloc(_microcode_manager->size * sizeof(Timer_t));

#ifdef DEBUG_MICROCODE
	if(_microcode_manager->engines == NULL)
		LOG_ERROR("Microcode manager: Failed allocating memory for gpioasm engines\r\n");
	if(_microcode_manager->timers == NULL)
		LOG_ERROR("Microcode manager: Failed allocating memory for gpioasm timers\r\n");
#endif

	  // start timer
	  HAL_TIM_Base_Start_IT(timer_handle);

	// init timers
	for (uint32_t i = 0; i < _microcode_manager->size; ++i) {
		_microcode_manager->timers[i].htim = timer_handle;
		_microcode_manager->timers[i].engine = _microcode_manager->engines + i;	// <=> *_ga_engines[i]
		_microcode_manager->timers[i].sequence_timeout_handler = gpioasm_handle_timer_timeout;
	}

	// init gpioasm
	for (uint32_t i = 0; i < _microcode_manager->size; ++i) {
		  gpioasm_init(_microcode_manager->engines + i, _gpioasm_init);
	}
}

void MicrocodeManager_start(MicrocodeManager_t *_microcode_manager, uint32_t index)
{
	// update microcode from Object Directory
	// TODO load new microcode as soon as recieved via callback function
	if(update_microcode_from_OD(_microcode_manager, index)) {

		// start microcode execution
		gpioasm_start(_microcode_manager->engines + index);
		return;
	}
#ifdef DEBUG_MICROCODE
	else{
		LOG_ERROR("Microcode manager: Failed loading microcode from OD\r\n");
	}
#endif
}

void MicrocodeManager_stop(MicrocodeManager_t *_microcode_manager, uint32_t index)
{
	// start microcode execution
	gpioasm_stop(_microcode_manager->engines + index);
}

void MicrocodeManager_restart(MicrocodeManager_t *_microcode_manager, uint32_t index)
{
	// start microcode execution
	MicrocodeManager_stop(_microcode_manager, index);

	gpioasm_reset(_microcode_manager->engines + index);

	MicrocodeManager_start(_microcode_manager, index);
}







