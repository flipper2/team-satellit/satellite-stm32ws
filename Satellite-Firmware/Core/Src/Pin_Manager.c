/*
 * Pin.c
 *
 *  Created on: Nov 29, 2023
 *      Author: Johannes Hofmann
 */

#include <CO_driver_target.h>
#include <main.h>
#include <Pin_Manager.h>
#include <stm32f4xx_hal_gpio.h>
#include <stm32f4xx_hal_tim.h>
#include <sys/_stdint.h>

void PinManager_init(PinManager_t *_pin_manager)
{
#ifdef DEBUG_PIN_MANAGER
	LOG_DEBUG("Pin Manager: Init function called");
#endif

#ifdef DIGITAL_INPUT_ENABLE
	_pin_manager->size_digital_input = 0;
	_pin_manager->digital_input = NULL;
#endif

#ifdef DIGITAL_OUTPUT_ENABLE
	_pin_manager->size_digital_output = 0;
	_pin_manager->digital_output = NULL;
#endif

#ifdef ANALOG_INPUT_ENABLE
	_pin_manager->size_analog_input = 0;
	pin_manager->analog_input = NULL;
#endif

#ifdef ANALOG_OUTPUT_ENABLE
	_pin_manager->size_analog_output = 0;
	_pin_manager->analog_output = NULL;
#endif
}

void PinManager_delete(PinManager_t* _pin_manager)
{
#ifdef DEBUG_PIN_MANAGER
	LOG_DEBUG("Pin manager: Delete function called\r\n");
#endif

	// delete Pins
#ifdef DIGITAL_INPUT_ENABLE
	for (uint8_t i = 0; i < _pin_manager->size_digital_input; ++i) {
			free(&_pin_manager->digital_input[i]);
	}
#endif

#ifdef DIGITAL_OUTPUT_ENABLE
	for (uint8_t i = 0; i < _pin_manager->size_digital_output; ++i) {
			free(&_pin_manager->digital_output[i]);
	}
#endif

#ifdef ANALOG_INPUT_ENABLE
	for (uint8_t i = 0; i < _pin_manager->size_analog_input; ++i) {
		free(&_pin_manager->analog_input[i]);
	}
#endif

#ifdef ANALOG_OUTPUT_ENABLE
	for (uint8_t i = 0; i < _pin_manager->size_analog_output; ++i) {
			free(&_pin_manager->analog_output[i]);
	}
#endif

	// delete pin_manager
	free(_pin_manager);
}

#ifdef DIGITAL_INPUT_ENABLE
void PinManager_addDigitalInputPin(PinManager_t *_pin_manager, Pin_t _pin) {
	// create new Pin8
	if(_pin_manager->size_digital_input == 0) {
		// first allocation with malloc
		_pin_manager->digital_input = (DigitalInput_t*)malloc(sizeof(DigitalInput_t));
	} else {
		// dynamic reallocation
		_pin_manager->digital_input = (DigitalInput_t*)realloc(_pin_manager->digital_input, (_pin_manager->size_digital_input+1) * sizeof(DigitalInput_t));
	}
	// increment size
	_pin_manager->size_digital_input++;

	// get newest element
	DigitalInput_t *di_pin = &_pin_manager->digital_input[_pin_manager->size_digital_input-1];

	// init gpio
	di_pin->pin = _pin;

	// set sub-index to next free entry
	di_pin->sub_index = _pin_manager->size_digital_input;	// sub-index[0] is 'Number of Elements' -> add 1

	// connect entries to OD
	ODR_t OD_Error = ODR_OK;

	di_pin->p_input_entry 	 = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_ENTRY), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_filter     = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_FILTER), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_polarity   = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_POLARITY), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_it_enable  = (bool_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_IT_ENABLE), 0x00, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_it_both    = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_IT_BOTH), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_it_rising  = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_IT_RISING), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_it_falling = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_IT_FALLING), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();
}

GPIO_PinState PinManager_readDigitalInputPin(PinManager_t *_pin_manager, uint32_t index)
{
	return (GPIO_PinState)*_pin_manager->digital_input[index].p_input_entry;
}
#endif


#ifdef DIGITAL_OUTPUT_ENABLE
void PinManager_addDigitalOutputPin(PinManager_t *_pin_manager, Pin_t _pin)
{
#ifdef DEBUG_PIN_MANAGER
	LOG_DEBUG("Pin manager: Add Digital Output function called\r\n");
#endif
	// create new Pin8
	if(_pin_manager->size_digital_output == 0) {
		// first allocation with malloc
		_pin_manager->digital_output = (DigitalOutput_t*)malloc(sizeof(DigitalOutput_t));
	} else {
		// dynamic reallocation
		_pin_manager->digital_output = (DigitalOutput_t*)realloc(_pin_manager->digital_output, (_pin_manager->size_digital_output+1) * sizeof(DigitalOutput_t));
	}
	// increment size
	_pin_manager->size_digital_output++;

	// get newest element
	DigitalOutput_t *do_pin = &_pin_manager->digital_output[_pin_manager->size_digital_output-1];

	// init gpio
	do_pin->pin  = _pin;

	// set sub-index to next free entry
	do_pin->sub_index = _pin_manager->size_digital_output;	// sub-index[0] is 'Number of Elements' -> add 1

	// connect entries to OD
	ODR_t OD_Error = ODR_OK;

	do_pin->p_output_entry 	 = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_ENTRY), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	do_pin->p_output_polarity     = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_POLARITY), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	do_pin->p_output_filter   = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_FILTER), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	do_pin->p_output_error_mode  = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_ERROR_MODE), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	do_pin->p_output_error_value    = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_ERROR_VALUE), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();
}

void PinManager_writeDigitalOutputPin(PinManager_t *_pin_manager, uint32_t index, GPIO_PinState state)
{
	*_pin_manager->digital_output[index].p_output_entry = (uint8_t)state;
}
#endif


#ifdef ANALOG_INPUT_ENABLE
void PinManager_addAnalogInputPin(PinManager_t *_pin_manager, Pin_t _pin)
{}

uint32_t PinManager_readAnalogInputPin(PinManager_t *_pin_manager, uint32_t index)
{}
#endif

#ifdef ANALOG_OUTPUT_ENABLE
void PinManager_addAnalogOutputPin(PinManager_t *_pin_manager, TIM_HandleTypeDef *htim, uint32_t channel)
{
	// create new Pin
	if(_pin_manager->size_analog_output == 0) {
		// first allocation with malloc
		_pin_manager->analog_output = (AnalogOutput_t*)malloc(sizeof(AnalogOutput_t));
	} else {
		// dynamic reallocation
		_pin_manager->analog_output = (AnalogOutput_t*)realloc(_pin_manager->analog_output, (_pin_manager->size_analog_output+1) * sizeof(AnalogOutput_t));
	}
	// increment size
	_pin_manager->size_analog_output++;

	AnalogOutput_t *ao_pin = &_pin_manager->analog_output[_pin_manager->size_analog_output-1];

	// init pin
//	ao_pin->pin = NULL;

	// init timer
	ao_pin->htim = htim;
	ao_pin->channel = channel;
	HAL_TIM_PWM_Start(htim, channel);

	// set sub-index to next free entry
	ao_pin->sub_index = _pin_manager->size_analog_output;	// sub-index[0] is 'Number of Elements' -> add 1


	// connect entries to OD
	ODR_t OD_Error = ODR_OK;

	ao_pin->p_value_8 	 = (int8_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_8), ao_pin->sub_index, sizeof(int8_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_value_16     = (int16_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_16), ao_pin->sub_index, sizeof(int16_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_value_32   = (int32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_32), ao_pin->sub_index, sizeof(int32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_value_float  = (float32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_FLOAT), ao_pin->sub_index, sizeof(float32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_offset_32    = (int32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_OFFSET_INTEGER), ao_pin->sub_index, sizeof(int32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_scaling_32    = (int32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_SCALING_INTEGER), ao_pin->sub_index, sizeof(int32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_offset_float    = (float32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_OFFSET_FLOAT), ao_pin->sub_index, sizeof(float32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_scaling_float    = (float32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_SCALING_FLOAT), ao_pin->sub_index, sizeof(float32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_error_mode    = (uint8_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_ERROR_MODE), ao_pin->sub_index, sizeof(uint8_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_error_value_32    = (uint32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_ERROR_VALUE_INTEGER), ao_pin->sub_index, sizeof(int32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_error_value_float    = (float32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_FLOAT), ao_pin->sub_index, sizeof(float32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();
}

void PinManager_writeAnalogOutputPin(PinManager_t *_pin_manager, uint32_t index, uint16_t duty_cycle)
{
	*_pin_manager->analog_output[index].p_value_8 = (uint8_t)duty_cycle;
}
#endif

