/*
 * CiA401.c
 *
 *  Created on: Nov 30, 2023
 *      Author: Johannes Hofmann
 */

#include "CiA401_Module.h"

#ifdef DIGITAL_INPUT_ENABLE
void CiA401_ProcessDigitalInput(void)
{
	uint8_t new_input_entry;
	DigitalInput_t *di;

	for (uint32_t i = 0; i < pin_manager.size_digital_input; ++i)
	{
		new_input_entry = false;
		di = &pin_manager.digital_input[i];

		// read GPIO pin
		new_input_entry = HAL_GPIO_ReadPin(di->pin.GPIO_Port, di->pin.GPIO_Pin);

		// TODO implement filter constant used in de-bounce filter

		// polarity change
		new_input_entry ^= *di->p_input_polarity;

		// check for changes
		if(*di->p_input_it_enable && (new_input_entry != *di->p_input_entry)) {
			//  input change occurred

			// generate interrupt mask (see doc for more info)
			uint8_t it_both 	= (new_input_entry ^ *di->p_input_entry) & *di->p_input_it_both;
			uint8_t it_rising 	= (new_input_entry & ~*di->p_input_entry) & *di->p_input_it_rising;
			uint8_t it_falling 	= (~new_input_entry & *di->p_input_entry) & *di->p_input_it_falling;

			uint8_t it = it_both | it_rising | it_falling;

			if(it) {
				// send TPDO
				CO_TPDOsendRequest(&canOpenNodeSTM32.canOpenStack->TPDO[i]);
			}
		}
		// update input entry in OD
		*di->p_input_entry = new_input_entry;
	}
}
#endif

#ifdef DIGITAL_OUTPUT_ENABLE
void CiA401_ProcessDigitalOutput(void)
{
	// TODO Device Error Handeling
	bool_t device_error = false;

	uint8_t output;
	DigitalOutput_t *dout;

	for (uint32_t i = 0; i < pin_manager.size_digital_output; ++i)
	{
		dout = &pin_manager.digital_output[i];

		if(!device_error) {
			output = *dout->p_output_entry;

		}else{
			if(*dout->p_output_error_mode == 0) {
				/* do nothing */
			}else{
				output = *dout->p_output_error_value;
			}
		}

		// change polarity
		output ^= *dout->p_output_polarity;

		// TODO filter

		// TODO check for changes
		if(1) {
			// write GPIO pin
			HAL_GPIO_WritePin(dout->pin.GPIO_Port, dout->pin.GPIO_Pin, output == 1 ? GPIO_PIN_SET : GPIO_PIN_RESET);
		}
	}
}
#endif

#ifdef ANALOG_INPUT_ENABLE
void CiA401_ProcessAnalogInput(void)
{
	// TODO implement CiA401_ProcessAnalogInput
}
#endif


#ifdef ANALOG_OUTPUT_ENABLE
void CiA401_ProcessAnalogOutput(void)
{
	// TODO Device Error Handeling
	bool_t device_error = false;

	// TODO support other value types
	uint8_t value8;
	AnalogOutput_t *ao;

	for (uint32_t i = 0; i < pin_manager.size_analog_output; ++i)
	{
		ao = &pin_manager.analog_output[i];

		if(!device_error) {
			value8 = *ao->p_value_8;

		}else{

			if(*ao->p_error_mode == 0) {
				/* do nothing */
			}else{
				value8 = (uint8_t)*ao->p_error_value_32;
			}
		}

		// check for changes
		if(ao->old_value_8 != value8) {
			__HAL_TIM_SET_COMPARE(ao->htim, ao->channel, (uint32_t)value8);	// invert output due to open drain configuration

			ao->old_value_8 = value8;
		}
	}
}
#endif
