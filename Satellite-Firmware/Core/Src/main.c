/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

// printf
#include  <errno.h>
#include  <sys/unistd.h> // STDOUT_FILENO, STDERR_FILENO

#include "configuration.h"

// CANOpen
#include "CO_app_STM32.h"
//#include "OD.h"
//#include "CANopen.h"

// CiA401
#include "Pin_Manager.h"
#include "CiA401_Module.h"

// Mikrocode
#include "microcode_manager.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

// Prevent CANOpen adding nodeID to TPDO COB-ID
#define CO_TPDO_DEFAULT_CANID_COUNT 0
#define CO_RPDO_DEFAULT_CANID_COUNT 0

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan2;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim10;
TIM_HandleTypeDef htim12;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

CANopenNodeSTM32 canOpenNodeSTM32;

PinManager_t pin_manager;

MicrocodeManager_t microcode_manager;

// for tpdo transmission overflow fix
uint8_t carry_flag[OD_CNT_TPDO] = {0};


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_CAN2_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM12_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM10_Init(void);
/* USER CODE BEGIN PFP */

void init_pins(PinManager_t *_pin_manager);
void microcode(void);

void tpdo_queue(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// printf
int _write(int file, char *data, int len)
{
   if ((file != STDOUT_FILENO) && (file != STDERR_FILENO))
   {
      errno = EBADF;
      return -1;
   }

   // arbitrary timeout 1000
   HAL_StatusTypeDef status =
      HAL_UART_Transmit(&huart1, (uint8_t*)data, len, 1000);

   // return # of bytes written - as best we can tell
   return (status == HAL_OK ? len : 0);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	// canopen
	if (htim == canopenNodeSTM32->timerHandle) {
		 canopen_app_interrupt();
	}
	// microcode timer
	if(htim == microcode_manager.timers[0].htim) {
		for (uint32_t i = 0; i < microcode_manager.size; ++i) {
			timer_sequence_step_callback(microcode_manager.timers + i);
		}
	}
}

// Mikrocode callback functions
void set_digital_output_pin(uint32_t index, uint8_t state){
    // state 0 = nff
    // state 1 = on
    // state 2 = high-impedance
    printf("setting digital pin %d to state %d\r\n", index, state);
    PinManager_writeDigitalOutputPin(&pin_manager, index, state);
}

void set_analog_output_pin(uint32_t index, uint16_t duty_cycle){
    printf("setting analog pin %d to state %d\r\n", index, duty_cycle);
    PinManager_writeAnalogOutputPin(&pin_manager, index, duty_cycle);
}

bool get_digital_input_pin(uint32_t index){
    return (bool)PinManager_readDigitalInputPin(&pin_manager, index);
}

void set_timer(void *engine_ptr, uint64_t timeout, bool start){

	uint32_t index = (engine_ptr - (void*)microcode_manager.engines) / sizeof(microcode_manager.engines[0]);

	if(start){

        printf("setting timer for %llums\r\n", timeout);
    	timer_sequence_start(microcode_manager.timers + index, timeout);

    }else{
        printf("stopping timer\r\n");
    	timer_sequence_stop(microcode_manager.timers + index);
    }
}

void CONodeFatalError(void)
{
  /* Place here your fatal error handling.
   * There is most likely a programming error.
   * !! Please don't ignore this errors. !!
   */
  for (;;);
}



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_CAN2_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM12_Init();
  MX_TIM5_Init();
  MX_TIM10_Init();
  /* USER CODE BEGIN 2 */

  /* Notes:
   * TIM10 -> CANOpen interrupt (Period = 1ms)
   * TIM5  -> Timer source for Microcode timer (Period = 1ms)
   */

  printf("---------------------\r\n\n");

  // node id
  uint8_t nodeID = 0;
  nodeID = 	  HAL_GPIO_ReadPin(ID_2_GPIO_Port, ID_2_Pin) << 2 |
		  	  HAL_GPIO_ReadPin(ID_1_GPIO_Port, ID_1_Pin) << 1 |
			  HAL_GPIO_ReadPin(ID_0_GPIO_Port, ID_0_Pin);

  // fix tpdo
  OD_PERSIST_COMM.x1800_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1801_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1802_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1803_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1804_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1805_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1806_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1807_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1808_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1809_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x180A_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x180B_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x180C_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x180D_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x180E_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x180F_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1810_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  OD_PERSIST_COMM.x1811_TPDOCommunicationParameter.COB_IDUsedByTPDO += nodeID;
  // fix new tpdo here

  // fix rpdo
  OD_PERSIST_COMM.x1400_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1401_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1402_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1403_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1404_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1405_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1406_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1407_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1408_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1409_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x140A_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x140B_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x140C_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x140D_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x140E_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x140F_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1410_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1411_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1412_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1413_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1414_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1415_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1416_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1417_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1418_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x1419_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x141A_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  OD_PERSIST_COMM.x141B_RPDOCommunicationParameter.COB_IDUsedByRPDO += nodeID;
  // fix new rpdo here


  // init CANOpen
  canOpenNodeSTM32.CANHandle = &hcan2;
  canOpenNodeSTM32.HWInitFunction = MX_CAN2_Init;
  canOpenNodeSTM32.timerHandle = &htim10;
  canOpenNodeSTM32.desiredNodeID = nodeID;
  canOpenNodeSTM32.baudrate = 125;	// -> 125 kbits/s

  canopen_app_init(&canOpenNodeSTM32);

  // prevent initial transmission of tpdo
  // Also in CO_PDO.c l.1444 prevent retransmission in not-operational mode
//  for (uint8_t i = 0; i < OD_CNT_TPDO; ++i) {
//	  canOpenNodeSTM32.canOpenStack->TPDO[i].sendRequest = false;
//  }

  // init pin manager
  PinManager_init(&pin_manager);

  init_pins(&pin_manager);

  // init microcode
  gpioasm_engine_init_t ga_init = {
		  .pin_digital_output_handler = set_digital_output_pin,
		  .pin_analog_output_handler = set_analog_output_pin,
		  .pin_digital_input_provider = get_digital_input_pin,
		  .timer_handler = set_timer
  };

  MicrocodeManager_init(&microcode_manager, &htim5, &ga_init, MICROCODE_ENGINE_COUNT);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  printf("Enter while...\r\n");

  while (1)
  {
	  // set CANOpen status leds
	  HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, canOpenNodeSTM32.outStatusLEDGreen);
	  HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, canOpenNodeSTM32.outStatusLEDRed);

	  // execute CiA401 processes
	  CiA401_ProcessDigitalInput();
	  CiA401_ProcessDigitalOutput();
	  CiA401_ProcessAnalogOutput();

	  // check for microcode execution rpdo
	  microcode();

	  // tpdo fix 2
	  tpdo_queue();

	  // execute CANOpen routine
	  canopen_app_process();

	  // wait 1ms
	  HAL_Delay(1);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CAN2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN2_Init(void)
{

  /* USER CODE BEGIN CAN2_Init 0 */

  /* USER CODE END CAN2_Init 0 */

  /* USER CODE BEGIN CAN2_Init 1 */

  /* USER CODE END CAN2_Init 1 */
  hcan2.Instance = CAN2;
  hcan2.Init.Prescaler = 20;
  hcan2.Init.Mode = CAN_MODE_NORMAL;
  hcan2.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan2.Init.TimeSeg1 = CAN_BS1_15TQ;
  hcan2.Init.TimeSeg2 = CAN_BS2_2TQ;
  hcan2.Init.TimeTriggeredMode = DISABLE;
  hcan2.Init.AutoBusOff = ENABLE;
  hcan2.Init.AutoWakeUp = ENABLE;
  hcan2.Init.AutoRetransmission = ENABLE;
  hcan2.Init.ReceiveFifoLocked = DISABLE;
  hcan2.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN2_Init 2 */

  /* USER CODE END CAN2_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 360-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 100-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 360-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 100-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 360-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 100-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 360-1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 100-1;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 180-1;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 1000-1;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief TIM10 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM10_Init(void)
{

  /* USER CODE BEGIN TIM10_Init 0 */

  /* USER CODE END TIM10_Init 0 */

  /* USER CODE BEGIN TIM10_Init 1 */

  /* USER CODE END TIM10_Init 1 */
  htim10.Instance = TIM10;
  htim10.Init.Prescaler = 180-1;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = 1000-1;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM10_Init 2 */

  /* USER CODE END TIM10_Init 2 */

}

/**
  * @brief TIM12 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM12_Init(void)
{

  /* USER CODE BEGIN TIM12_Init 0 */

  /* USER CODE END TIM12_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM12_Init 1 */

  /* USER CODE END TIM12_Init 1 */
  htim12.Instance = TIM12;
  htim12.Init.Prescaler = 360-1;
  htim12.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim12.Init.Period = 100-1;
  htim12.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim12.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim12) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim12, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim12) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim12, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim12, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM12_Init 2 */

  /* USER CODE END TIM12_Init 2 */
  HAL_TIM_MspPostInit(&htim12);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LED_RED_Pin|LED_GREEN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CAN_SILENT_GPIO_Port, CAN_SILENT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : GPIO_6_Pin GPIO_15_Pin GPIO_16_Pin GPIO_14_Pin
                           GPIO_4_Pin GPIO_13_Pin GPIO_5_Pin GPIO_8_Pin
                           GPIO_17_Pin GPIO_7_Pin */
  GPIO_InitStruct.Pin = GPIO_6_Pin|GPIO_15_Pin|GPIO_16_Pin|GPIO_14_Pin
                          |GPIO_4_Pin|GPIO_13_Pin|GPIO_5_Pin|GPIO_8_Pin
                          |GPIO_17_Pin|GPIO_7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : GPIO_3_Pin GPIO_11_Pin GPIO_12_Pin GPIO_10_Pin
                           GPIO_9_Pin GPIO_0_Pin GPIO_2_Pin GPIO_1_Pin */
  GPIO_InitStruct.Pin = GPIO_3_Pin|GPIO_11_Pin|GPIO_12_Pin|GPIO_10_Pin
                          |GPIO_9_Pin|GPIO_0_Pin|GPIO_2_Pin|GPIO_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_RED_Pin LED_GREEN_Pin */
  GPIO_InitStruct.Pin = LED_RED_Pin|LED_GREEN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : CAN_SILENT_Pin */
  GPIO_InitStruct.Pin = CAN_SILENT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CAN_SILENT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : ID_2_Pin */
  GPIO_InitStruct.Pin = ID_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ID_2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : ID_1_Pin ID_0_Pin */
  GPIO_InitStruct.Pin = ID_1_Pin|ID_0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

void init_pins(PinManager_t *_pin_manager)
{
	// debugging
//	PinManager_addDigitalOutputPin(_pin_manager, (Pin_t){LED_RED_Pin, LED_RED_GPIO_Port});

	// add digital input pins
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_0_Pin, GPIO_0_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_1_Pin, GPIO_1_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_2_Pin, GPIO_2_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_3_Pin, GPIO_3_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_4_Pin, GPIO_4_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_5_Pin, GPIO_5_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_6_Pin, GPIO_6_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_7_Pin, GPIO_7_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_8_Pin, GPIO_8_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_9_Pin, GPIO_9_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_10_Pin, GPIO_10_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_11_Pin, GPIO_11_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_12_Pin, GPIO_12_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_13_Pin, GPIO_13_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_14_Pin, GPIO_14_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_15_Pin, GPIO_15_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_16_Pin, GPIO_16_GPIO_Port});
	PinManager_addDigitalInputPin(_pin_manager, (Pin_t){GPIO_17_Pin, GPIO_17_GPIO_Port});

	// change polarity because inputs are pull-up (inverted)
	for (uint8_t i = 0; i < _pin_manager->size_digital_input; ++i) {
		*_pin_manager->digital_input[i].p_input_polarity = 0x01;
	}

	// add analog output pins
	PinManager_addAnalogOutputPin(_pin_manager, &htim12, TIM_CHANNEL_2);	// COIL 0
	PinManager_addAnalogOutputPin(_pin_manager, &htim12, TIM_CHANNEL_1);	// COIL 1
	PinManager_addAnalogOutputPin(_pin_manager, &htim3, TIM_CHANNEL_1);		// COIL 2
	PinManager_addAnalogOutputPin(_pin_manager, &htim3, TIM_CHANNEL_2);		// COIL 3
	PinManager_addAnalogOutputPin(_pin_manager, &htim3, TIM_CHANNEL_3);		// COIL 4
	PinManager_addAnalogOutputPin(_pin_manager, &htim3, TIM_CHANNEL_4);		// COIL 5
	PinManager_addAnalogOutputPin(_pin_manager, &htim1, TIM_CHANNEL_4);		// COIL 6
	PinManager_addAnalogOutputPin(_pin_manager, &htim1, TIM_CHANNEL_1);		// COIL 7
	PinManager_addAnalogOutputPin(_pin_manager, &htim4, TIM_CHANNEL_1);		// COIL 8
	PinManager_addAnalogOutputPin(_pin_manager, &htim4, TIM_CHANNEL_2);		// COIL 9
	PinManager_addAnalogOutputPin(_pin_manager, &htim2, TIM_CHANNEL_1);		// COIL 10
	PinManager_addAnalogOutputPin(_pin_manager, &htim2, TIM_CHANNEL_2);		// COIL 11
}

void microcode()
{
	// microcode 0
	if(OD_RAM.x7001_microcodeControl.microcode_Control_0 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_0 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_0) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x00); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x00); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x00); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_0 = 255;
	}

	// microcode 1
	if(OD_RAM.x7001_microcodeControl.microcode_Control_1 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_1 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_1) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x01); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x01); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x01); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_1 = 255;
	}

	// microcode 2
	if(OD_RAM.x7001_microcodeControl.microcode_Control_2 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_2 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_2) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x02); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x02); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x02); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_2 = 255;
	}

	// microcode 3
	if(OD_RAM.x7001_microcodeControl.microcode_Control_3 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_3 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_3) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x03); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x03); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x03); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_3 = 255;
	}

	// microcode 4
	if(OD_RAM.x7001_microcodeControl.microcode_Control_4 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_4 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_4) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x04); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x04); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x04); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_4 = 255;
	}

	// microcode 5
	if(OD_RAM.x7001_microcodeControl.microcode_Control_5 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_5 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_5) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x05); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x05); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x05); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_5 = 255;
	}

	// microcode 6
	if(OD_RAM.x7001_microcodeControl.microcode_Control_6 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_6 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_6) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x06); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x06); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x06); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_6 = 255;
	}

	// microcode 7
	if(OD_RAM.x7001_microcodeControl.microcode_Control_7 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_7 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_7) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x07); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x07); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x07); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_7 = 255;
	}

	// microcode 8
	if(OD_RAM.x7001_microcodeControl.microcode_Control_8 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_8 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_8) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x08); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x08); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x08); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_8 = 255;
	}

	// microcode 9
	if(OD_RAM.x7001_microcodeControl.microcode_Control_9 == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_9 >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_9) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x09); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x09); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x09); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_9 = 255;
	}

	// microcode 10
	if(OD_RAM.x7001_microcodeControl.microcode_Control_a == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_a >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_a) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x0a); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x0a); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x0a); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_a = 255;
	}

	// microcode 11
	if(OD_RAM.x7001_microcodeControl.microcode_Control_b == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_b >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_b) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x0b); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x0b); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x0b); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_b = 255;
	}

	// microcode 12
	if(OD_RAM.x7001_microcodeControl.microcode_Control_c == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_c >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_c) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x0c); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x0c); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x0c); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_c = 255;
	}

	// microcode 13
	if(OD_RAM.x7001_microcodeControl.microcode_Control_d == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_d >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_d) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x0d); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x0d); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x0d); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_d = 255;
	}

	// microcode 14
	if(OD_RAM.x7001_microcodeControl.microcode_Control_e == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_e >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_e) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x0e); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x0e); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x0e); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_e = 255;
	}

	// microcode 15
	if(OD_RAM.x7001_microcodeControl.microcode_Control_f == 255 ||
		OD_RAM.x7001_microcodeControl.microcode_Control_f >= MICROCODE_ENGINE_COUNT) {
	}else{
		switch(OD_RAM.x7001_microcodeControl.microcode_Control_f) {
		case 0: MicrocodeManager_stop(&microcode_manager, 0x0f); break;
		case 1: MicrocodeManager_start(&microcode_manager, 0x0f); break;
		case 2: MicrocodeManager_restart(&microcode_manager, 0x0f); break;
		default: /* should not happen */  break;
		}
		OD_RAM.x7001_microcodeControl.microcode_Control_f = 255;
	}
}

// checks how many tpdo are queed
// send first 2 tpdos
void tpdo_queue(void)
{
	uint8_t count = 0;

	for (uint8_t i = 0; i < OD_CNT_TPDO; ++i) {
		if(carry_flag[i] == 1 && canOpenNodeSTM32.canOpenStack->TPDO[i].sendRequest == true) {
			// change nothing
		}
		else if(carry_flag[i] == 1 || canOpenNodeSTM32.canOpenStack->TPDO[i].sendRequest == true){
			canOpenNodeSTM32.canOpenStack->TPDO[i].sendRequest = true;
			carry_flag[i] = 0;
		}else{
			canOpenNodeSTM32.canOpenStack->TPDO[i].sendRequest = false;
			carry_flag[i] = 0;
		}
	}


	for (uint8_t i = 0; i < OD_CNT_TPDO; ++i) {

		if(canOpenNodeSTM32.canOpenStack->TPDO[i].sendRequest == true) {
			if(count >= 1) {
				canOpenNodeSTM32.canOpenStack->TPDO[i].sendRequest = false;
				carry_flag[i] = 1;
			}else{
				count++;
			}
		}
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
