/*
 * Pin.h
 *
 *  Created on: Nov 29, 2023
 *      Author: Johannes Hofmann
 */

#ifndef INC_PIN_MANAGER_H_
#define INC_PIN_MANAGER_H_

// includes
#include <stdlib.h>
#include <string.h>

#include "stm32f4xx_hal.h"

#include "301/CO_ODinterface.h"

extern OD_t *OD;

// only 8-bit mode supported yet
// TODO support 1-bit, 16-bit and 32-bit mode

// defines
/// Digital Input
#define OD_DIGITAL_INPUT_ENTRY 			(uint16_t)0x6000
#define OD_DIGITAL_INPUT_POLARITY 		(uint16_t)0x6002
#define OD_DIGITAL_INPUT_FILTER 		(uint16_t)0x6003
#define OD_DIGITAL_INPUT_IT_ENABLE 		(uint16_t)0x6005
#define OD_DIGITAL_INPUT_IT_BOTH		(uint16_t)0x6006
#define OD_DIGITAL_INPUT_IT_FALLING		(uint16_t)0x6007
#define OD_DIGITAL_INPUT_IT_RISING		(uint16_t)0x6008

/// Digital Output
#define OD_DIGITAL_OUTPUT_ENTRY 		(uint16_t)0x6200
#define OD_DIGITAL_OUTPUT_POLARITY 		(uint16_t)0x6202
#define OD_DIGITAL_OUTPUT_ERROR_MODE 	(uint16_t)0x6206
#define OD_DIGITAL_OUTPUT_ERROR_VALUE 	(uint16_t)0x6207
#define OD_DIGITAL_OUTPUT_FILTER 		(uint16_t)0x6208

// Analog Output
#define OD_ANALOG_OUTPUT_VALUE_8				(uint16_t)0x6410
#define OD_ANALOG_OUTPUT_VALUE_16				(uint16_t)0x6411
#define OD_ANALOG_OUTPUT_VALUE_32				(uint16_t)0x6412
#define OD_ANALOG_OUTPUT_VALUE_FLOAT			(uint16_t)0x6413
#define OD_ANALOG_OUTPUT_OFFSET_FLOAT			(uint16_t)0x6441
#define OD_ANALOG_OUTPUT_SCALING_FLOAT			(uint16_t)0x6442
#define OD_ANALOG_OUTPUT_OFFSET_INTEGER			(uint16_t)0x6446
#define OD_ANALOG_OUTPUT_SCALING_INTEGER		(uint16_t)0x6447
#define OD_ANALOG_OUTPUT_ERROR_MODE				(uint16_t)0x6443
#define OD_ANALOG_OUTPUT_ERROR_VALUE_FLOAT		(uint16_t)0x6445
#define OD_ANALOG_OUTPUT_ERROR_VALUE_INTEGER	(uint16_t)0x6444


// typedefs
typedef enum
{
	Digital_Input = 0,
	Digital_Output,
	Analog_Input,
	Analog_Output
} Pin_t;

typedef struct
{
	// GPIO
	uint16_t GPIO_Pin;
	GPIO_TypeDef *GPIO_Port;

} Pin_t;

typedef struct
{
	GPIO_InitTypeDef GPIO_InitPin;
	GPIO_TypeDef *GPIO_Port;

} Pin_InitTypeDef;


typedef struct
{
	// GPIO
	uint8_t pins_used;
	uint16_t GPIO_Pin[8];
	GPIO_TypeDef *GPIO_Port[8];

} Pin8;

typedef struct
{
	uint8_t pins_used;
	GPIO_InitTypeDef GPIO_InitPin[8];
	GPIO_TypeDef *GPIO_Port[8];

} Pin8_InitTypeDef;

typedef struct {
	Pin8 pin8;

	uint8_t sub_index;

	uint8_t *p_input_entry;
	uint8_t *p_input_filter;
	uint8_t *p_input_polarity;
	bool_t  *p_input_it_enable;
	uint8_t *p_input_it_both;
	uint8_t *p_input_it_rising;
	uint8_t *p_input_it_falling;

} DigitalInput8;

typedef struct {
	Pin8 pin8;

	uint8_t sub_index;

	uint8_t *p_output_entry;
	uint8_t *p_output_polarity;
	uint8_t *p_output_filter;
	uint8_t *p_output_error_mode;
	uint8_t *p_output_error_value;

} DigitalOutput8;

//typedef struct {
//	Pin8;
//
//	uint8_t sub_index;
//
//	uint8_t *p_output_entry;
//	uint8_t *p_output_polarity;
//	uint8_t *p_output_filter;
//	uint8_t *p_output_error_mode;
//	uint8_t *p_output_error_value;
//
//} AnalogInput8;

typedef struct {

	// GPIO Pin
	Pin_t pin;

	// Timer
	TIM_HandleTypeDef *htim;
	uint32_t channel;

	// CANOpen
	uint8_t sub_index;

	int8_t *p_value_8;
	int16_t *p_value_16;
	int32_t *p_value_32;
	float32_t *p_value_float;

	int16_t old_value_8;

	int32_t *p_offset_32;
	int32_t *p_scaling_32;
	float32_t *p_offset_float;
	float32_t *p_scaling_float;

	uint8_t *p_error_mode;
	uint32_t *p_error_value_32;
	float32_t *p_error_value_float;

} AnalogOutput;


typedef struct
{
	uint8_t total_amount_pins;

	DigitalInput8 *digital_input8;
	DigitalOutput8 *digital_output8;
//	AnalogInput *analog_input;
	AnalogOutput *analog_output;

	uint8_t size_digital_input8;
	uint8_t size_digital_output8;
	uint8_t size_analog_input;
	uint8_t size_analog_output;

} PinManager;

// Pin Manager
PinManager* PinManager_init();
void PinManager_delete(PinManager* pin_manager);

void PinManager_addDigitalInputPin8(PinManager *_pin_manager, Pin8_InitTypeDef *_pin8_init);
void PinManager_addDigitalOutputPin8(PinManager *_pin_manager, Pin8_InitTypeDef *_pin8_init);

void PinManager_addAnalogInputPin(PinManager *_pin_manager, Pin_InitTypeDef *_pin_init);
void PinManager_addAnalogOutputPin(PinManager *_pin_manager, Pin_InitTypeDef *_pin_init, TIM_HandleTypeDef *htim, uint32_t channel);

// TIM Base IRQ


#endif /* INC_PIN_MANAGER_H_ */







