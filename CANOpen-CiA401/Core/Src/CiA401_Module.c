/*
 * CiA401.c
 *
 *  Created on: Nov 30, 2023
 *      Author: Johannes Hofmann
 */

#include "CiA401_Module.h"

void CiA401_ProcessDigitalInput(void)
{
	uint8_t new_input_entry;
	DigitalInput8 *di8;

	for (uint32_t i = 0; i < pin_manager->size_digital_input8; ++i)
	{
		new_input_entry = 0;
		di8 = &pin_manager->digital_input8[i];

		// read GPIO pins
		for (uint8_t k = 0; k < di8->pin8.pins_used; ++k)
		{
			new_input_entry = modify_bit(new_input_entry, k, (uint8_t)HAL_GPIO_ReadPin(di8->pin8.GPIO_Port[k], di8->pin8.GPIO_Pin[k]));
		}

		// TODO implement filter constant used in de-bounce filter

		// polarity change
		new_input_entry ^= *di8->p_input_polarity;

		// check for changes
		if(*di8->p_input_it_enable && new_input_entry != *di8->p_input_entry) {
			//  input change occurred

			// generate interrupt mask (see doc for more info)
			uint8_t it_both 	= (new_input_entry ^ *di8->p_input_entry) & *di8->p_input_it_both;
			uint8_t it_rising 	= (new_input_entry & ~*di8->p_input_entry) & *di8->p_input_it_rising;
			uint8_t it_falling 	= (~new_input_entry & *di8->p_input_entry) & *di8->p_input_it_falling;

			uint8_t it = it_both | it_rising | it_falling;

			if(it) {
				// send TPDO[0] -> Digitial Inputs
				CO_TPDOsendRequest(&canOpenNodeSTM32.canOpenStack->TPDO[0]);
			}

			// update input entry in OD
			*di8->p_input_entry = new_input_entry;
		}
	}
}


void CiA401_ProcessDigitalOutput(void)
{
	// TODO Device Error Handeling
	bool_t device_error = false;

	uint8_t output;
	DigitalOutput8 *do8;

	for (uint32_t i = 0; i < pin_manager->size_digital_output8; ++i)
	{
		do8 = &pin_manager->digital_output8[i];

		if(!device_error) {
			output = *do8->p_output_entry;

		}else{
			if(*do8->p_output_error_mode == 0) {
				/* do nothing */
			}else{
				output = *do8->p_output_error_value;
			}
		}

		// change polarity
		output ^= *do8->p_output_polarity;

		// TODO filter

		// TODO check for changes
		if(1) {
			// write GPIO pins
			for (uint8_t k = 0; k < do8->pin8.pins_used; ++k)
			{
				HAL_GPIO_WritePin(do8->pin8.GPIO_Port[k], do8->pin8.GPIO_Pin[k], (output >> k) & 0x01);
			}
		}
	}
}

void CiA401_ProcessAnalogInput(void)
{
	// TODO implement CiA401_ProcessAnalogInput
}

void CiA401_ProcessAnalogOutput(void)
{
	// TODO Device Error Handeling
	bool_t device_error = false;

	// TODO support other value types
	uint8_t value8;
	AnalogOutput *ao;

	for (uint32_t i = 0; i < pin_manager->size_analog_output; ++i)
	{
		ao = &pin_manager->analog_output[i];

		if(!device_error) {
			value8 = *ao->p_value_8;

		}else{

			if(*ao->p_error_mode == 0) {
				/* do nothing */
			}else{
				value8 = (uint8_t)*ao->p_error_value_32;
			}
		}

		// check for changes
		if(ao->old_value_8 != value8) {
			__HAL_TIM_SET_COMPARE(ao->htim, ao->channel, (uint32_t)value8);

			ao->old_value_8 = value8;
		}
	}
}
