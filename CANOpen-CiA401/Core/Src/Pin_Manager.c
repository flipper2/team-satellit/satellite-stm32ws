/*
 * Pin.c
 *
 *  Created on: Nov 29, 2023
 *      Author: Johannes Hofmann
 */

#include "Pin_Manager.h"

/////////////////////  HELPERS  ////////////////////

void init_pin(Pin_t *_pin, Pin_InitTypeDef *_pin_init)
{
	// copy Pin/Port
	_pin->GPIO_Pin  = (uint16_t)_pin_init->GPIO_InitPin.Pin;
	_pin->GPIO_Port = _pin_init->GPIO_Port;

	// init HAL Pin
	HAL_GPIO_Init(_pin_init->GPIO_Port, &_pin_init->GPIO_InitPin);
}

void init_Pin8(Pin8 *_pin8, Pin8_InitTypeDef *_pin8_init) {

	// init Pin8
	_pin8->pins_used = _pin8_init->pins_used;

	// init GPIO stuff
	for (uint8_t i = 0; i < _pin8->pins_used; ++i) {
		_pin8->GPIO_Pin[i] = (uint16_t)_pin8_init->GPIO_InitPin[i].Pin;
	}
	memcpy(_pin8->GPIO_Port, _pin8_init->GPIO_Port, _pin8->pins_used * sizeof(GPIO_TypeDef *));

	// init HAL Pin
	for (uint8_t i = 0; i < _pin8->pins_used; ++i) {
		HAL_GPIO_Init(_pin8_init->GPIO_Port[i], &_pin8_init->GPIO_InitPin[i]);
	}
}


///////////////////////////////////////////////////

PinManager* PinManager_init()
{
	PinManager *pin_manager = (PinManager*)malloc(sizeof(PinManager));

	pin_manager->size_digital_input8 = 0;
	pin_manager->size_digital_output8 = 0;
	pin_manager->size_analog_input = 0;
	pin_manager->size_analog_output = 0;

	pin_manager->digital_input8 = NULL;
	pin_manager->digital_output8 = NULL;
//	pin_manager->analog_input = NULL;
	pin_manager->analog_output = NULL;
	// TODO init pins

	return pin_manager;
}

void PinManager_delete(PinManager* _pin_manager)
{
	// delete Pins
	for (uint8_t i = 0; i < _pin_manager->size_digital_input8; ++i) {
		free(&_pin_manager->digital_input8[i]);
	}
	for (uint8_t i = 0; i < _pin_manager->size_digital_output8; ++i) {
		free(&_pin_manager->digital_output8[i]);
	}
//	for (uint8_t i = 0; i < _pin_manager->size_analog_input; ++i) {
//		free(&_pin_manager->analog_input[i]);
//	}
	for (uint8_t i = 0; i < _pin_manager->size_analog_output; ++i) {
		free(&_pin_manager->analog_output[i]);
	}

	// delete pin_manager
	free(_pin_manager);
}

void PinManager_addDigitalInputPin8(PinManager *_pin_manager, Pin8_InitTypeDef *_pin8_init)
{
	// create new Pin8
	if(_pin_manager->size_digital_input8 == 0) {
		// first allocation with malloc
		_pin_manager->digital_input8 = (DigitalInput8*)malloc(sizeof(DigitalInput8));
	} else {
		// dynamic reallocation
		_pin_manager->digital_input8 = (DigitalInput8*)realloc(_pin_manager->digital_input8, (_pin_manager->size_digital_input8+1) * sizeof(DigitalInput8));
	}
	// increment size
	_pin_manager->size_digital_input8++;

	// get newest element
	DigitalInput8 *di_pin = &_pin_manager->digital_input8[_pin_manager->size_digital_input8-1];

	// init gpio
	init_Pin8(&di_pin->pin8, _pin8_init);

	// set sub-index to next free entry
	di_pin->sub_index = _pin_manager->size_digital_input8;	// sub-index[0] is 'Number of Elements' -> add 1

	// connect entries to OD
	ODR_t OD_Error;

	di_pin->p_input_entry 	 = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_ENTRY), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_filter     = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_FILTER), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_polarity   = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_POLARITY), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_it_enable  = (bool_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_IT_ENABLE), 0x00, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_it_both    = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_IT_BOTH), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_it_rising  = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_IT_RISING), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	di_pin->p_input_it_falling = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_INPUT_IT_FALLING), di_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();
}

void PinManager_addDigitalOutputPin8(PinManager *_pin_manager, Pin8_InitTypeDef *_pin8_init)
{
	// create new Pin8
	if(_pin_manager->size_digital_output8 == 0) {
		// first allocation with malloc
		_pin_manager->digital_output8 = (DigitalOutput8*)malloc(sizeof(DigitalOutput8));
	} else {
		// dynamic reallocation
		_pin_manager->digital_output8 = (DigitalOutput8*)realloc(_pin_manager->digital_output8, (_pin_manager->size_digital_output8+1) * sizeof(DigitalOutput8));
	}
	// increment size
	_pin_manager->size_digital_output8++;

	// get newest element
	DigitalOutput8 *do_pin = &_pin_manager->digital_output8[_pin_manager->size_digital_output8-1];

	// init gpio
	init_Pin8(&do_pin->pin8, _pin8_init);

	// set sub-index to next free entry
	do_pin->sub_index = _pin_manager->size_digital_output8;	// sub-index[0] is 'Number of Elements' -> add 1

	// connect entries to OD
	ODR_t OD_Error;

	do_pin->p_output_entry 	 = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_ENTRY), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	do_pin->p_output_polarity     = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_POLARITY), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	do_pin->p_output_filter   = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_FILTER), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	do_pin->p_output_error_mode  = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_ERROR_MODE), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	do_pin->p_output_error_value    = (uint8_t*)OD_getPtr(OD_find(OD, OD_DIGITAL_OUTPUT_ERROR_VALUE), do_pin->sub_index, 1, &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();
}

void PinManager_addAnalogOutputPin(PinManager *_pin_manager, Pin_InitTypeDef *_pin_init, TIM_HandleTypeDef *htim, uint32_t channel)
{
	// create new Pin
	if(_pin_manager->size_analog_output == 0) {
		// first allocation with malloc
		_pin_manager->analog_output = (AnalogOutput*)malloc(sizeof(AnalogOutput));
	} else {
		// dynamic reallocation
		_pin_manager->analog_output = (AnalogOutput*)realloc(_pin_manager->analog_output, (_pin_manager->size_analog_output+1) * sizeof(AnalogOutput));
	}
	// increment size
	_pin_manager->size_analog_output++;

	AnalogOutput *ao_pin = &_pin_manager->analog_output[_pin_manager->size_analog_output-1];

	// init pin
//	init_pin(&ao_pin->pin, _pin_init);

	// init timer
	ao_pin->htim = htim;
	ao_pin->channel = channel;
	HAL_TIM_PWM_Start(htim, channel);

	// set sub-index to next free entry
	ao_pin->sub_index = _pin_manager->size_analog_output;	// sub-index[0] is 'Number of Elements' -> add 1


	// connect entries to OD
	ODR_t OD_Error;

	ao_pin->p_value_8 	 = (int8_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_8), ao_pin->sub_index, sizeof(int8_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_value_16     = (int16_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_16), ao_pin->sub_index, sizeof(int16_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_value_32   = (int32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_32), ao_pin->sub_index, sizeof(int32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_value_float  = (float32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_FLOAT), ao_pin->sub_index, sizeof(float32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_offset_32    = (int32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_OFFSET_INTEGER), ao_pin->sub_index, sizeof(int32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_scaling_32    = (int32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_SCALING_INTEGER), ao_pin->sub_index, sizeof(int32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_offset_float    = (float32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_OFFSET_FLOAT), ao_pin->sub_index, sizeof(float32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_scaling_float    = (float32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_SCALING_FLOAT), ao_pin->sub_index, sizeof(float32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_error_mode    = (uint8_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_ERROR_MODE), ao_pin->sub_index, sizeof(uint8_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_error_value_32    = (uint32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_ERROR_VALUE_INTEGER), ao_pin->sub_index, sizeof(int32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();

	ao_pin->p_error_value_float    = (float32_t*)OD_getPtr(OD_find(OD, OD_ANALOG_OUTPUT_VALUE_FLOAT), ao_pin->sub_index, sizeof(float32_t), &OD_Error);
	if(OD_Error != ODR_OK) Error_Handler();
}




















